import argparse
import os
import pwd
import shutil
import subprocess
import sys


def main(jar,
         base_directory, user, force_skel, service_file, namespace, psr4_base,
         template_directory, docs_directory, src_directory, tests_directory,
         git_host, pkg_namespace, pkg_name,
         author_name, author_email, author_url, package_license,
         name_map):
    output_directory = os.path.join(template_dir, 'output')
    try:
        os.mkdir(output_directory)
    except FileExistsError:
        pass

    print(f"Building php library for {service_file} into the {namespace} namespace.")
    cmd_args = [
        f"--invoker-package={namespace}",
        f"--git-host={git_host}",
        f"--git-repo-id={pkg_name}",
        f"--git-user-id={pkg_namespace}",
        '--global-property', 'skipFormModel=false',
        '--package-name', pkg_name,
        '-p', f"srcBasePath={src_directory}",
        # Hack test path because it's not configurable.
        '-p', f"testBasePath2={tests_directory}",
        '-g', 'php',
    ]
    if jar:
        cmd = [
            'java',
            '-Dlog.level=error',
            '-jar',
            jar
        ]
        cmd_args = cmd_args + [
            '-o', output_directory,
            '-t', f'{template_directory}/template',
        ]

    else:
        cmd = [
            'docker',
            'run',
            '--rm',
            '-v', f"{template_directory}:/openapi-php/",
            '-v', f"{output_directory}:/output/",
            '-e', 'JAVA_OPTS=-Dlog.level=error',
            'openapitools/openapi-generator-cli:latest-release',
        ]
        cmd_args = cmd_args + [
            '-o', '/output',
            '-t', '/openapi-php/template',
        ]

    if os.path.exists(f"{base_directory}/{service_file}"):
        if jar:
            service_file = f"{base_directory}/{service_file}"
        else:
            cmd = cmd + ['-v', f"{base_directory}:/source/"]
            service_file = f"/source/{service_file}"
    cmd_args  = cmd_args + ['-i', service_file]

    cmd = cmd + ['generate'] + cmd_args
    if package_license:
        cmd.append('--additional-properties')
        cmd.append(f'licenseName={package_license}')
    if author_name:
        cmd.append('--additional-properties')
        cmd.append(f'developerOrganization={author_name}')
    if author_email:
        cmd.append('--additional-properties')
        cmd.append(f'developerEmail={author_email}')
    if author_email:
        cmd.append('--additional-properties')
        cmd.append(f'developerOrganizationUrl={author_url}')
    cmd.append('--skip-validate-spec')
    # https://github.com/openapitools/openapi-generator/blob/master/docs/customization.md#name-mapping
    # TODO handle name, property, model, and enum name mapping
    if name_map:
        for map in name_map:
            cmd.append('--name-mappings')
            cmd.append(map)

    print(" ".join(cmd))
    subprocess.run(cmd)

    if psr4_base:
        namespace_parts = namespace.replace(psr4_base, '').lstrip('\\').split('\\')
    else:
        namespace_parts = []

    process_dir(os.path.join(output_directory, 'docs', 'Api'), os.path.join(base_directory, docs_directory, 'Api'))
    process_dir(os.path.join(output_directory, 'docs', 'Model'), os.path.join(base_directory, docs_directory, 'Model'))
    process_dir(os.path.join(output_directory, src_directory), os.path.join(*[base_directory, src_directory] + namespace_parts))
    process_dir(os.path.join(output_directory, 'test'), os.path.join(*[base_directory, tests_directory] + namespace_parts))

    skel_file(os.path.join(output_directory, 'composer.json'), os.path.join(base_directory, 'composer.json'), force_skel)
    skel_file(os.path.join(output_directory, 'README.md'), os.path.join(base_directory, 'README.md'), force_skel)
    skel_file(os.path.join(output_directory, '.gitignore'), os.path.join(base_directory, '.gitignore'), force_skel)
    skel_file(os.path.join(output_directory, 'phpunit.xml.dist'), os.path.join(base_directory, 'phpunit.xml.dist'), force_skel)
    for root, dirs, files in os.walk(os.path.join(template_dir, 'skel')):
        for path in files:
            skel_file(os.path.join(root, path), os.path.join(base_directory, path), force_skel)

    # Docker process runs as root so all the output files will be owned by root
    # after it finishes. This looks up who setup the output directory and
    # converts ownership. If the wrapping script did this as a unprivileged
    # user and run this as a privileged user everything will work out fine.
    if user:
        pwd_user = pwd.getpwnam(user)
        chown_all(base_directory, pwd_user.pw_uid, pwd_user.pw_gid)

    # Cleanup.
    shutil.rmtree(output_directory)


def skel_file(source, dest, force):
    if not os.path.exists(source):
        return

    # Don't overwrite an existing file.
    if os.path.exists(dest) and not force:
        return

    print(" ".join(["Copying files: ", source, dest]))
    shutil.copy(source, dest)

def process_dir(source, dest):
    if not os.path.isdir(source):
        return
    # Ensure directory exists.
    if not os.path.isdir(dest):
        os.makedirs(dest)

    # Make sure directory is empty.
    try:
        shutil.rmtree(dest)
    except FileNotFoundError:
        pass
    os.rename(source, dest)


def chown_all(directory, uid, gid):
    if not uid or not gid:
        print("missing user id")
        return

    try:
        os.chown(directory, uid, gid)
        for root, dirs, files in os.walk(directory):
            for path in dirs:
                # print("Fixing " + os.path.join(root, path) + f"({uid}, {gid})")
                os.chown(os.path.join(root, path), uid, gid)
            for path in files:
                # print("Fixing " + os.path.join(root, path) + f"({uid}, {gid})")
                os.chown(os.path.join(root, path), uid, gid)
    except PermissionError:
        print("Permission denied. You may need to run the script with sudo.", file=sys.stderr)


if __name__ == "__main__":
    template_dir = os.path.dirname(os.path.abspath(__file__))
    parser = argparse.ArgumentParser(description="Compile source.")
    parser.add_argument("--jar", default=None, type=str, help="Use a specific jar when generating. If not provided, will try to use official docker image.")
    parser.add_argument("--base-directory", default=os.getcwd(), type=str)
    parser.add_argument("--namespace", default=None, type=str, required=True)
    parser.add_argument("--psr4-base", default='', type=str)
    parser.add_argument("--user", default=None, type=str)
    parser.add_argument("--service-file", default=None, type=str, required=True)
    parser.add_argument("--template-directory", default=template_dir, type=str)
    parser.add_argument("--docs-directory", default="docs", type=str)
    parser.add_argument("--src-directory", default="src", type=str)
    parser.add_argument("--tests-directory", default="tests", type=str)
    parser.add_argument("--pkg-namespace", default="default", type=str)
    parser.add_argument("--pkg-name", default="default-api-php", type=str)
    parser.add_argument("--git-host", default="github.com", type=str)
    parser.add_argument("--force-skel", action='store_true', help="Force replacement of skeleton files")
    parser.add_argument("--license", default='unlicense', type=str, dest='package_license', help="License name")
    parser.add_argument("--author-name", default='', type=str, help="Author or organization name")
    parser.add_argument("--author-email", default='', type=str, help="Author or organization email")
    parser.add_argument("--author-url", default='', type=str, help="Author or organization website url")
    parser.add_argument("--name-map", default=[], type=str, action='append', help="Map name conflicts. should be in the form 'originalName=newName'")

    args = parser.parse_args()
    main(**vars(args))
