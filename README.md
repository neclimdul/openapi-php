# Open API PHP Templates
PHP OpenAPI Generator templates based on the internal templates.

This originally started out as fixing type hints for static analysis tools when generated libraries are used. For
example if a parameter hints string in the doc block but accepts integers, when the calling code in your software passes
the integer it is flagged as a bug even if it is correct.

Static tests started pretty quickly finding little bugs which lead to some fixes, and a _lot_ of code cleanups as tools
started flagging other parts of the code.

At this point, a lot of static analysis problems have been resolved and this is focused on compatibility, updates and cleanups.

## Goals
  - Faster development
  - Bug fixes
  - Psalm/PhpStan compatibility.
  - Additional spec compatibility
  - Testing

## Usage
The project is just a template built on top of the OpenAPI Generator project PHP generator. There is an included `build.py` script that wraps an easy the process of compiling a project with the specification.

Additionally, as part of the goal of hardening the code there is an aggressive adoption of PHP's new type features. Some of that has been buggy so there are two branches available to mitigate that.

 1) The `main` branch has more aggressive adoption of types including method return typing. Sometimes this runs into bugs with the generator though.
 2) The `v1` branch is less aggressive with types and avoids method return types based on your spec. This is generally "safer" in that it runs into fewer bugs and will be a bit more generous with weird or buggy specs.

### Example
Here's a sample script you can adjust to build your project.

```sh 
#!/bin/sh

build_openapi() {
    SERVICE=${1}
    NAMESPACE=${2}
    sudo python3 ./openapi-php/build.py \
        --user=${USER} \
        --namespace="OpenApi\\${NAMESPACE}" \
        --psr4-base="OpenApi" \
        --service-file=${SERVICE} \
        --docs-directory="${PWD}/docs/${NAMESPACE}" \
        --src-directory="${PWD}/src/" \
        --tests-directory="${PWD}/tests/"
}

# Download/sync templates
if [ ! -d "$PWD/openapi-php" ]; then
    git clone git@gitlab.com:neclimdul/openapi-php.git
else
    cd openapi-php || exit
    git pull
    cd - || exit
fi

# sync your specs if needed.
if [ ! -d "$PWD/OpenAPI-Specification" ]; then
  git clone https://github.com/OAI/OpenAPI-Specification.git
else
  cd OpenAPI-Specification || exit
  git pull
  cd - || exit
fi

build_openapi "examples/v2.0/json/petstore.json" "PetStoreLocal"

# Alternatively you cna just pass a URL to build function instead of a local path.
build_openapi "https://petstore.swagger.io/v2/swagger.json" "PetStoreRemote"

```
