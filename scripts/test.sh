#!/bin/sh

build()
{
    INPUT=${1}
    OUTPUT=build/${2}
    build_openapi "$INPUT" "$OUTPUT"
    setup "$OUTPUT"
    runtest "$OUTPUT"
}

build_openapi() {
    SERVICE=${1}
    NAMESPACE="Test"
    OUTPUT="$PWD/${2}"
    sudo python3 ./build.py \
        --user="${USER}" \
        --service-file="${SERVICE}" \
        --namespace="OpenApi\\${NAMESPACE}" \
        --base-directory="${OUTPUT}"
}

setup ()
{
    OUTPUT=${1}
    cd "${OUTPUT}" \
        && composer install -q
    cd -
}

runtest()
{
    INPUT=${1}
    cd "${INPUT}" \
        && composer lint \
        && composer phpcs || composer phpcbf || composer phpcs \
        && composer static \
        && composer test
    cd -
}


# Start fresh
rm -rf build
mkdir build -p

build https://petstore.swagger.io/v2/swagger.json pet-v2
#exit;
build https://petstore3.swagger.io/api/v3/openapi.json pet-v3
build https://developers.marketo.com/swagger/swagger-mapi.json mkto-lead
build https://developers.marketo.com/swagger/swagger-asset.json mkto-asset
build https://developers.marketo.com/swagger/swagger-identity.json mkto-identity
#build test.yml test
